package main

import (
	"context"
	desc "currency_rates/pkg/api"
	"currency_rates/services/currency-rest-service/pkg/services/currency"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

const grpcPort = 50051

type server struct {
	desc.UnimplementedCurrencyServer
}

func (s *server) Add(ctx context.Context, req *desc.CurrencyRequest) (*desc.CurrencyResponse, error) {
	log.Println(req.Request)
	c := currency.Currency{}
	c.New()
	err := c.Add(req.Request)
	if err != nil {
		log.Println(err)
		return &desc.CurrencyResponse{Result: 0}, err
	}
	return &desc.CurrencyResponse{Result: 1}, nil
}

func main() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", grpcPort))
	if err != nil {
		log.Fatal(err)
	}
	s := grpc.NewServer()
	reflection.Register(s)
	desc.RegisterCurrencyServer(s, &server{})

	if err = s.Serve(lis); err != nil {
		log.Fatal(err)
	}
}
