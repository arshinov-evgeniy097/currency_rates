package main

import (
	"currency_rates/services/currency-parser-service"
	grpc_client "currency_rates/services/currency-parser-service/pkg/grpc-client"
	"currency_rates/services/currency-parser-service/pkg/providers/nbrb"
)

func main() {
	provider := new(nbrb.Nbrb)
	gc := grpc_client.GrpcClient{}
	gc.Init()
	provider.GrpcClient = gc.Client
	p := new(currency_parser_service.Parser)
	p.Run(provider)
}
