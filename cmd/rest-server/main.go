package main

import (
	api "currency_rates/services/currency-rest-service"
)

func main() {
	r := api.NewRuntime()
	r.Start()
	r.Wait()
	r.Stop()
	r.Shutdown()
}
