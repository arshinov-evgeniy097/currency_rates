FROM golang:1.22.3 AS builder
WORKDIR /app/parser
COPY . .
RUN go mod download
COPY ./cmd/parser/ .
RUN go build -o rest-server main.go

FROM scratch AS rest-server
COPY --from=builder /app/parser/parser /parser

CMD ["/parser"]