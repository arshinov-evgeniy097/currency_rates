package repositories

import (
	"currency_rates/services/currency-rest-service/internal/domain/entities/currency"
)

type CurrencyRepositoriesInteface interface {
	Add(rows []currency.Currency) error
	GetAll() ([]currency.Currency, error)
	GetByDate(date string) ([]currency.Currency, error)
}
