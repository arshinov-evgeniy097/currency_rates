package memory

import (
	"currency_rates/services/currency-rest-service/internal/domain/entities/currency"
)

type MemoryRepository struct {
}

func (mr *MemoryRepository) Add(rows []currency.Currency) {
}

func (mr *MemoryRepository) GetAll() ([]currency.Currency, error) {
	return nil, nil
}

func (mr *MemoryRepository) GetByDate(date string) ([]currency.Currency, error) {
	return nil, nil
}
