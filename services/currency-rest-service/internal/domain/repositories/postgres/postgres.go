package postgres

import (
	"currency_rates/services/currency-rest-service/internal/domain/entities/currency"
	"currency_rates/services/currency-rest-service/internal/infrastructure/db"
	"log"
)

type PostgressRepository struct {
	Db db.Postgres
}

func (pr *PostgressRepository) New(db db.Postgres) *PostgressRepository {
	return &PostgressRepository{Db: db}
}

func (pr *PostgressRepository) Add(rows []currency.Currency) error {
	conn := pr.Db.GetDBConnection()
	defer conn.Close()
	_, err := conn.Table("currency_rates.rates").Insert(rows)
	if err != nil {
		return err
	}
	return nil
}

func (pr *PostgressRepository) GetAll() ([]currency.Currency, error) {
	conn := pr.Db.GetDBConnection()
	defer conn.Close()
	var cs []currency.Currency
	err := conn.Table("currency_rates.rates").Find(&cs)
	if err != nil {
		log.Fatal(err)
	}
	return cs, nil
}

func (pr *PostgressRepository) GetByDate(date string) ([]currency.Currency, error) {
	conn := pr.Db.GetDBConnection()
	defer conn.Close()
	var cs []currency.Currency
	err := conn.Table("currency_rates.rates").Where("`date` like '?%'", date).Find(&cs)
	if err != nil {
		log.Fatal(err)
	}
	return cs, nil
}
