package interfaces

import (
	"currency_rates/services/currency-rest-service/internal/domain/entities/currency"
)

type CurrencySliceInterface interface {
	Add(currency currency.Currency) error
	AddSlice([]currency.Currency) error
	GetAll() []currency.Currency
}
