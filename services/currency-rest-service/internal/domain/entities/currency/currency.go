package currency

import "errors"

type Currency struct {
	CID          int     `json:"Cur_ID" xorm:"cid"`
	Date         string  `json:"Date" xorm:"date"`
	Abbreviation string  `json:"Cur_Abbreviation" xorm:"abbreviation"`
	Scale        int     `json:"Cur_Scale" xorm:"scale"`
	Name         string  `json:"Cur_Name" xorm:"name"`
	OfficialRate float32 `json:"Cur_OfficialRate" xorm:"officeal_rate"`
}

const (
	ERR_CURR_CID          = "CID if reqired field"
	ERR_CURR_DATE         = "Date if reqired field"
	ERR_CURR_ABBREVIATION = "Abbreviation if reqired field"
	ERR_CURR_SCALE        = "Scale if reqired field"
	ERR_CURR_NAME         = "Name if reqired field"
	ERR_CURR_OR           = "OfficialRate if reqired field"
)

func (c *Currency) validate(currency Currency) error {
	if currency.CID == 0 {
		return errors.New(ERR_CURR_CID)
	}
	if currency.Date == "" {
		return errors.New(ERR_CURR_DATE)
	}
	if currency.Abbreviation == "" {
		return errors.New(ERR_CURR_ABBREVIATION)
	}
	if currency.Scale == 0 {
		return errors.New(ERR_CURR_SCALE)
	}
	if currency.Name == "" {
		return errors.New(ERR_CURR_NAME)
	}
	if currency.OfficialRate == 0 {
		return errors.New(ERR_CURR_OR)
	}

	return nil
}

func (c *Currency) NewCurrency(currency Currency) error {
	if err := c.validate(currency); err != nil {
		return err
	}
	*c = currency
	return nil
}
