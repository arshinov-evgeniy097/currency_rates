package currency

import (
	"reflect"
	"testing"
)

func TestCurrencies_New(t *testing.T) {

}

func TestCurrencies_GetAll(t *testing.T) {
	type fields struct {
		Currencies Currencies
	}
	tests := []struct {
		name   string
		fields fields
		want   Currencies
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Currencies{
				Currencies: tt.fields.Currencies,
			}
			if got := c.GetAll(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAll() = %v, want %v", got, tt.want)
			}
		})
	}
}
