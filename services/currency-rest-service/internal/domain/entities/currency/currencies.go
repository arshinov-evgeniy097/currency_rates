package currency

type Currencies struct {
	currencySlice []Currency
}

func (cs *Currencies) Create() *Currencies {
	return &Currencies{}
}

func (cs *Currencies) Add(currency Currency) error {
	err := currency.NewCurrency(currency)
	if err != nil {
		return err
	}
	cs.currencySlice = append(cs.currencySlice, currency)
	return nil
}

func (cs *Currencies) AddSlice(currencySlice []Currency) error {
	if len(currencySlice) != 0 {
		for _, cur := range currencySlice {
			err := cs.Add(cur)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (cs *Currencies) GetAll() []Currency {
	return cs.currencySlice
}
