package db

import (
	"currency_rates/services/currency-rest-service/internal/config"
	"errors"
	"fmt"
	_ "github.com/lib/pq"
	"xorm.io/xorm"
)

type Postgres struct {
	engine *xorm.Engine
	Config config.Config
}

const ERR_DB = "ERROR, engine is nil %s"

func (p *Postgres) New() error {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		p.Config.DbHost, p.Config.DbPort, p.Config.DbUser, p.Config.DbPassword, p.Config.DbName)
	db, err := xorm.NewEngine("postgres", psqlInfo)
	if err != nil {
		return err
	}
	if db == nil {
		return errors.New(fmt.Sprintf(ERR_DB, p.Config.DbHost))
	}
	if err = db.Ping(); err != nil {
		return err
	}
	p.engine = db

	return nil
}

func (p *Postgres) GetDBConnection() xorm.Engine {
	for i := 0; i < 10; i++ {
		if err := p.engine.Ping(); err != nil {
			err := p.New()
			if err != nil {
				panic(err)
			}
		} else {
			break
		}
	}

	return *p.engine
}
