package interfaces

import (
	"currency_rates/services/currency-rest-service/internal/config"
	"xorm.io/xorm"
)

type DbInterface interface {
	New(config config.Config) error
	GetDBConnection() xorm.Engine
}
