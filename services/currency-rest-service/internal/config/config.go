package config

import (
	"github.com/AbsaOSS/env-binder/env"
	"log"
)

type Config struct {
	DbHost     string `env:"POSTGRES_HOST, require=true"`
	DbPort     int    `env:"POSTGRES_PORT, require=true"`
	DbUser     string `env:"POSTGRES_USER, require=true"`
	DbPassword string `env:"POSTGRES_PASSWORD, require=true"`
	DbName     string `env:"POSTGRES_DB, require=true"`
}

func (c Config) InitConfig() (Config, error) {
	err := env.Bind(&c)
	if err != nil {
		return Config{}, err
	}
	log.Println(c)
	return c, nil
}
