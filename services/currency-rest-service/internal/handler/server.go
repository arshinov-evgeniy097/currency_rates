package handler

import (
	"currency_rates/services/currency-rest-service/internal/infrastructure/db"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
)

type ApiServer struct {
	server http.Server
	engine *gin.Engine
	Db     db.Postgres
	Logger *logrus.Logger
}

func (as *ApiServer) Run() {
	as.Init()
	err := as.engine.Run(":8086")
	if err != nil {
		as.Logger.Fatal(err)
	}
}
