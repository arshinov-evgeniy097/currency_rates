package handler

import (
	"currency_rates/services/currency-rest-service/internal/domain/repositories/postgres"
	"github.com/gin-gonic/gin"
)

func (as *ApiServer) Init() {
	pr := &postgres.PostgressRepository{Db: as.Db}
	cc := CurrencyHandler{DbService: as.Db, Repo: pr, Logger: as.Logger}
	mux := gin.New()
	mux.Handle("GET", "/getAll", cc.GetAll)
	mux.Handle("GET", "/getByDate", cc.GetByDate)
	as.engine = mux
}
