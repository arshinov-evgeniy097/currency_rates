package handler

import (
	"currency_rates/services/currency-rest-service/internal/domain/repositories"
	"currency_rates/services/currency-rest-service/internal/infrastructure/db"
	"currency_rates/services/currency-rest-service/pkg/services/currency"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type CurrencyHandler struct {
	DbService db.Postgres
	Repo      repositories.CurrencyRepositoriesInteface
	service   currency.Currency
	Logger    *logrus.Logger
}

func (ch *CurrencyHandler) Init() {
	ch.service = currency.Currency{DbService: ch.DbService, Logger: ch.Logger}
}

func (ch *CurrencyHandler) GetAll(ctx *gin.Context) {
	res := ch.service.GetAll()
	ctx.String(200, string(res))
}

func (ch *CurrencyHandler) GetByDate(ctx *gin.Context) {
	date := ctx.GetString("date")
	res := ch.service.GetByDate(date)
	ctx.String(200, string(res))
}
