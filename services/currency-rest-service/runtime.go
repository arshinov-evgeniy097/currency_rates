package currency_rest_service

import (
	"currency_rates/services/currency-rest-service/internal/config"
	currency_api "currency_rates/services/currency-rest-service/internal/handler"
	"currency_rates/services/currency-rest-service/internal/infrastructure/db"
	"github.com/sirupsen/logrus"
	"log"
)

type Runtime struct {
	Db     db.Postgres
	Logger *logrus.Logger
}

func NewRuntime() *Runtime {
	return &Runtime{}
}

func (r *Runtime) Start() {
	l := logrus.New()
	r.Logger = l

	r.Logger.Info("initial config")
	c, err := config.Config{}.InitConfig()
	if err != nil {
		log.Fatal(err)
	}

	r.Logger.Info("initial db")
	pdb := db.Postgres{Config: c}
	err = pdb.New()
	if err != nil {
		log.Fatal(err)
	}
	r.Logger.Info("initial rest-server")
	server := currency_api.ApiServer{Db: pdb, Logger: r.Logger}

	r.Logger.Info("run rest-server")
	server.Run()
}

func (r *Runtime) Stop() {

}

func (r *Runtime) Wait() {

}

func (r *Runtime) Shutdown() {

}
