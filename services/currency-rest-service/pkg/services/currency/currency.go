package currency

import (
	"currency_rates/services/currency-rest-service/internal/config"
	"currency_rates/services/currency-rest-service/internal/domain/entities/currency"
	"currency_rates/services/currency-rest-service/internal/domain/repositories"
	"currency_rates/services/currency-rest-service/internal/infrastructure/db"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"log"
)

type Currency struct {
	DbService db.Postgres
	Repo      repositories.CurrencyRepositoriesInteface
	Logger    *logrus.Logger
}

func (cc *Currency) New() Currency {
	l := logrus.New()
	cc.Logger = l

	c, err := config.Config{}.InitConfig()
	if err != nil {
		log.Fatal(err)
	}

	pdb := db.Postgres{Config: c}
	err = pdb.New()
	if err != nil {
		log.Fatal(err)
	}
	cc.DbService = pdb
	return *cc
}

func (cc *Currency) GetAll() []byte {
	cs, err := cc.Repo.GetAll()
	if err != nil {
		cc.Logger.Error(err)
		return nil
	}
	res, err := json.Marshal(cs)

	if err != nil {
		cc.Logger.Error(err)
		return nil
	}
	return res
}

func (cc *Currency) GetByDate(date string) []byte {
	cs, err := cc.Repo.GetByDate(date)
	if err != nil {
		cc.Logger.Fatal(err)
	}
	res, err := json.Marshal(cs)

	if err != nil {
		cc.Logger.Fatal(err)
	}
	return res
}

func (cc *Currency) Add(data []byte) error {
	var cs []currency.Currency
	err := json.Unmarshal(data, &cs)
	if err != nil {
		cc.Logger.Error(err)
		return err
	}
	err = cc.Repo.Add(cs)
	if err != nil {
		cc.Logger.Error(err)
		return err
	}

	return nil
}
