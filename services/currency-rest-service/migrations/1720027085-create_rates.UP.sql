-- +migrate Up
create schema currency_rates;

create table IF NOT EXISTS currency_rates.rates
(
    id  serial primary key,
    cid           integer,
    date          varchar(255),
    abbreviation  varchar(255),
    scale         integer,
    name          varchar(255),
    officeal_rate double precision
);

alter table currency_rates.rates
    owner to currency_rates_user;

-- +migrate Down
drop table currency_rates.rates;
drop schema currency_rates;
