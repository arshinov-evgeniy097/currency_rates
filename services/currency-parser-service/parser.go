package currency_parser_service

import (
	"currency_rates/services/currency-parser-service/pkg/providers/interfaces"
)

type Parser struct {
}

func (p *Parser) Run(provider interfaces.ProviderInterface) {
	result := provider.Parse()
	provider.Send(result)
}
