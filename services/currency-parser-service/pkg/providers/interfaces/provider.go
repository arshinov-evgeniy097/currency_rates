package interfaces

type ProviderInterface interface {
	Parse() []byte
	Send([]byte)
}
