package nbrb

import (
	"context"
	"currency_rates/pkg/api"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"time"
)

const NbnrApi = "https://api.nbrb.by/exrates/rates?periodicity=0"
const ErrApiStatusCode = "Response nbrb not 200, actual status %s"

type Nbrb struct {
	GrpcClient api.CurrencyClient
}

func (nbrb *Nbrb) Create() *Nbrb {
	return &Nbrb{}
}

func (nbrb *Nbrb) Parse() []byte {
	res, err := http.Get(NbnrApi)
	if err != nil {
		log.Fatal(err)
	}
	if res.StatusCode != 200 {
		log.Fatal(ErrApiStatusCode, res.Status)
	}

	b, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	return b
}

func (nbrb *Nbrb) Send(result []byte) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	_, err := nbrb.GrpcClient.Add(ctx, &api.CurrencyRequest{
		Request: result,
	})
	if err != nil {
		log.Println(err)
	}
}
