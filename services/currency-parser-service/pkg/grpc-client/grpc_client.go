package grpc_client

import (
	"currency_rates/pkg/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

type GrpcClient struct {
	Client api.CurrencyClient
}

const grpcHost = "localhost:50051"

func (g *GrpcClient) Init() {
	conn, err := grpc.NewClient(grpcHost, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal(err)
	}
	c := api.NewCurrencyClient(conn)
	g.Client = c
}
