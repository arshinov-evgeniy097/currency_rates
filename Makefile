migrations:
	docker-compose up currency_migrations


PHONY: generate-cur
generate-cur:
	mkdir -p pkg/api
	protoc --go_out=pkg --go_opt=paths=source_relative --go-grpc_out=pkg api/currency.proto